# Template Node.js

Este template serve unicamente para reuso para novas aplicações escritas em Node.js.

Sempre que for reutilizar, verificar a possibilidade de atualização dos arquivos e diretorios.

# Dawnload

Faça o uso a vontade e sem restrição, fique a vontade para atualizar e fazer suas melhoras.

# Rodando

Use o comando do yarn para fazer a instalação das depedências.

# Configuração

## husky

Após rodar o comando para instalar as dependencias, vá no diretorio: .husky/ e adicione um arquivo com nome: pre-commit

Nele adicione o seguinte:

```bash
#!/usr/bin/env sh
. "$(dirname -- "$0")/_/husky.sh"

yarn lint-staged
```

## Eslint

Depois haverá alguns erros de eslint, basta corrigir que o projeto estará 100%

# Melhorias

versão: 1.0.2
- [Atualização de README.md]

# Criado por:

- [@moisesfauto](https://beacons.ai/moisesfausto)
