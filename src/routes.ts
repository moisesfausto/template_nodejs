import multer from '@config/multer'
import Router from 'express'

// Importação das ClassControllers

const router = Router()

// Exemplo de rotas
router.get('/exemplo', ClassController.index)
router.post('/exemplo', ClassController.store)
router.get('/exemplo/:id', ClassController.show)
router.put('/exemplo/:id', ClassController.update)
router.delete('/exemplo/:id', ClassController.delete)


export { router }
