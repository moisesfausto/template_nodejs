import DailyRotateFile from 'winston-daily-rotate-file'
import winston from 'winston'

const transport: DailyRotateFile = new DailyRotateFile({
	dirname: './logs',
	filename: 'application.log.%DATE%',
	maxSize: '10m',
	maxFiles: '7d'
})

const loggerService = winston.createLogger({
	level: 'info',
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.printf(({timestamp, level, message}) => {
			return `${[timestamp]} ${level.toUpperCase()}: ${message}`
		}),
	),
	transports: [
		transport
	],
})

export default loggerService
