export class HandleErrors extends Error{
	public readonly status: number
	constructor(message: string, status: number) {
		super(message)
		this.status = status
	}
}

export class BadRequestError extends HandleErrors {
	constructor(message: string) {
		super(message, 400)
	}
}

export class NotFoundError extends HandleErrors {
	constructor(message: string) {
		super(message, 404)
	}
}

export class UnauthorizedError extends HandleErrors {
	constructor(message: string) {
		super(message, 401)
	}
}
